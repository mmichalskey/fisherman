﻿namespace Fisherman
{
    partial class MainWindow
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.bFishing = new System.Windows.Forms.Button();
            this.bSelectFishingRegion = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lFishCout = new System.Windows.Forms.Label();
            this.cbLure = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // bFishing
            // 
            this.bFishing.Location = new System.Drawing.Point(13, 12);
            this.bFishing.Name = "bFishing";
            this.bFishing.Size = new System.Drawing.Size(122, 24);
            this.bFishing.TabIndex = 0;
            this.bFishing.Text = "Fishing";
            this.bFishing.UseVisualStyleBackColor = true;
            this.bFishing.Click += new System.EventHandler(this.bFishing_Click);
            // 
            // bSelectFishingRegion
            // 
            this.bSelectFishingRegion.Location = new System.Drawing.Point(13, 42);
            this.bSelectFishingRegion.Name = "bSelectFishingRegion";
            this.bSelectFishingRegion.Size = new System.Drawing.Size(122, 24);
            this.bSelectFishingRegion.TabIndex = 2;
            this.bSelectFishingRegion.Text = "Select fishing region";
            this.bSelectFishingRegion.UseVisualStyleBackColor = true;
            this.bSelectFishingRegion.Click += new System.EventHandler(this.bSelectFishingRegion_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(141, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Fishes:";
            // 
            // lFishCout
            // 
            this.lFishCout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lFishCout.Location = new System.Drawing.Point(141, 46);
            this.lFishCout.Name = "lFishCout";
            this.lFishCout.Size = new System.Drawing.Size(46, 15);
            this.lFishCout.TabIndex = 4;
            this.lFishCout.Text = "0";
            this.lFishCout.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cbLure
            // 
            this.cbLure.AutoSize = true;
            this.cbLure.Location = new System.Drawing.Point(13, 72);
            this.cbLure.Name = "cbLure";
            this.cbLure.Size = new System.Drawing.Size(150, 17);
            this.cbLure.TabIndex = 5;
            this.cbLure.Text = "Lure already on fishing rod";
            this.cbLure.UseVisualStyleBackColor = true;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(199, 96);
            this.Controls.Add(this.cbLure);
            this.Controls.Add(this.lFishCout);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bSelectFishingRegion);
            this.Controls.Add(this.bFishing);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Fisherman";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bFishing;
        private System.Windows.Forms.Button bSelectFishingRegion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lFishCout;
        private System.Windows.Forms.CheckBox cbLure;
    }
}

