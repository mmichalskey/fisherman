﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Fisherman
{
    public partial class MainWindow : Form
    {
        private int XL, YT, XR, YB;
        private System.Timers.Timer fishing_timer;
        private System.Timers.Timer lure_timer;
        private NAudio.CoreAudioApi.MMDeviceEnumerator devEnum;
        bool fishing = false;
        int fish_counter = 0;
        private Task t;

        private void bSelectFishingRegion_Click(object sender, EventArgs e)
        {
            Region region = new Region();
            region.BackColor = Color.Lime;
            region.TransparencyKey = Color.Lime;
            if (region.ShowDialog() == DialogResult.Cancel)
            {
                XL = region.Bounds.Left;
                YT = region.Bounds.Top + 25;
                XR = region.Bounds.Right;
                YB = region.Bounds.Bottom;
            }
        }

        private void UpdateFishCout()
        {
            MethodInvoker inv = delegate
            {
                this.lFishCout.Text = this.fish_counter.ToString();
            };

            this.Invoke(inv);
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Lure(Object source, System.Timers.ElapsedEventArgs e)
        {
            fishing_timer.Stop();
            SendKeys.SendWait("2");
            Thread.Sleep(4000);
            SendKeys.SendWait("3");
            Thread.Sleep(4000);
            fishing_timer.Start();

        }

        private Sailor.Point FindBobber()
        {
            Sailor.Point p = new Sailor.Point();
            for (int y = YT; y <= YB; y += 8)
                for (int x = XL; x <= XR; x += 22)
                {
                    Thread.Sleep(10);
                    Sailor.MoveMouse(x, y);

                    var cursorInfo = Sailor.GetCurrentCursor();
                    var cursorBmp = Sailor.GetCursorIcon(cursorInfo);
                    if (cursorBmp.ImageCompare(Properties.Fisherman.Cursor))
                    {
                        p.x = x;
                        p.y = y + 20;
                        return p;
                    }
                }
            return p;
        }

        private void Reset(System.Timers.Timer timer)
        {
            timer.Stop();
            timer.Start();
        }

        private void Fishing(Object source, System.Timers.ElapsedEventArgs e)
        {
            UpdateFishCout();
            SendKeys.SendWait("1");

            //if bobber not found after 22-28 sec fishing will be call
            var random = new Random(DateTime.Now.Millisecond);
            int rnd = random.Next(22000, 28000);
            fishing_timer.Interval = rnd;

            var bobber = FindBobber();

            if (!bobber.Equals(null))
            {
                t = Task.Run(() =>
                {
                    bool fish = false;
                    while (true)
                    {
                        var devices = devEnum.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.All, NAudio.CoreAudioApi.DeviceState.Active);
                        var value = (int)(Math.Round(devices[0].AudioMeterInformation.MasterPeakValue * 100));
                        if (value > 45 && !fish)
                        {
                            fish = true;
                            Sailor.LeftMouseClick(bobber.x, bobber.y);
                            //Thread.(500);
                            fish_counter++;
                            //Sailor.MoveMouse(XL, YT);
                            random = new Random(DateTime.Now.Millisecond);
                            rnd = random.Next(1300, 1600);
                            fishing_timer.Interval = rnd;
                            break;
                        }

                        Thread.Sleep(50);
                    }

                });
                t.Dispose();
                
                
            }
        }

        private void bFishing_Click(object sender, EventArgs e)
        {
            Thread.Sleep(1000);
            if (!cbLure.Checked)
            { 
                SendKeys.SendWait("2");
                Thread.Sleep(5000);
                SendKeys.SendWait("3");
                Thread.Sleep(5000);
            }
            if (fishing == false)
            {
                devEnum = new NAudio.CoreAudioApi.MMDeviceEnumerator();

                lure_timer = new System.Timers.Timer();
                lure_timer.Interval = 580000;
                lure_timer.Elapsed += Lure;
                lure_timer.AutoReset = true;
                lure_timer.Enabled = true;

                fishing_timer = new System.Timers.Timer();
                fishing_timer.Interval = 2100;
                fishing_timer.Elapsed += Fishing;
                fishing_timer.AutoReset = true;
                fishing_timer.Enabled = true;
                fishing = true;

            }
            else
            {
                fishing = false;
                fishing_timer.Enabled = false;
                lure_timer.Enabled = false;
                devEnum.Dispose();
            }
                
        }
    }
}
